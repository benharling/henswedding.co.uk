'use-strict';


var gulp = require('gulp');
var sass = require('gulp-sass');
// var coffee = require('gulp-coffee');
var gutil = require('gulp-util');
var bourbon_paths = require('bourbon').includePaths;
var livereload = require('gulp-livereload');

var html_folders = [
	'./cms_templates/**/*.html', 
	'./wedding_food/**/*.html'
]

gulp.task('sass', function () {
	return gulp.src('./henswedding/static/scss/**/*.scss')
		.pipe(sass({includePaths: bourbon_paths}).on('error', sass.logError))
		.pipe(gulp.dest('./henswedding/static/css'))
		.pipe(livereload());
});

// gulp.task('coffee', function () {
// 	gulp.src('./core/static/coffee/**/*.coffee')
// 		.pipe(coffee().on('error', gutil.log))
// 		.pipe(gulp.dest('./core/static/js'))
// 		.pipe(livereload());
// });

gulp.task('html', function () {
	return gulp.src(html_folders)
		.pipe(livereload());
})


gulp.task('default', function () {
	livereload.listen();
	gulp.watch('./henswedding/static/scss/**/*.scss', ['sass']);
	// gulp.watch('./core/static/coffee/**/*.coffee', ['coffee']);
	gulp.watch(html_folders, ['html']);
});