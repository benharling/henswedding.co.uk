from django.contrib import admin

# Register your models here.

from wedding_food.models import Dish, Promise


class PromiseAdmin(admin.ModelAdmin):
	list_display = ['name', 'dish']

class PromiseInline(admin.TabularInline):
	model = Promise


class DishAdmin(admin.ModelAdmin):
	list_display = ['title', 'serves', 'num_required' , 'total_promised', 'portions_promised']
	list_editable = ['num_required',]
	inlines = [PromiseInline, ]

admin.site.register(Dish, DishAdmin)

admin.site.register(Promise, PromiseAdmin)