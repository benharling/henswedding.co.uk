# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-05-21 12:55
from __future__ import unicode_literals

from django.db import migrations, models
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wedding_food', '0004_dish_serves'),
    ]

    operations = [
        migrations.AddField(
            model_name='dish',
            name='ingredients',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AddField(
            model_name='dish',
            name='method',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='dish',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(blank=True, default=''),
        ),
    ]
