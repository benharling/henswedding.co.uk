from __future__ import unicode_literals

from django.db import models
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
from django.utils.six import python_2_unicode_compatible
from djangocms_text_ckeditor.fields import HTMLField
from django.core.urlresolvers import reverse


# Create your models here.

@python_2_unicode_compatible
class Dish(models.Model):

	title = models.CharField(max_length=128, blank=True, default='')
	description = HTMLField(default='', blank=True)

	ingredients = models.TextField(default='', blank=True)
	method = models.TextField(default='', blank=True)

	link = models.URLField(null=True, blank=True)
	image = FilerImageField(
		null=True, blank=True, related_name="dish_picture"
	)

	difficulty = models.PositiveIntegerField(default=2)
	num_required = models.PositiveIntegerField(default=1)
	serves = models.PositiveIntegerField(default=2)

	def get_absolute_url(self):
		return reverse(
			'dish-detail',
			kwargs= {
				'pk':self.pk
			}
		)

	def total_promised(self):
		return "{} / {}".format(self.promise_set.count(), self.num_required)

	def portions_promised(self):
		return self.serves * self.promise_set.count()

	@property
	def available(self):
		return max(0, self.num_required - self.promise_set.count())

	def remaining(self):
		return [""] * self.available

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class Promise(models.Model):
	name = models.CharField(max_length=128, null=True, blank=True)
	dish = models.ForeignKey(Dish)
	email = models.EmailField(null=True, blank=True)

	def __str__(self):
		return self.name or self.dish.title or 'Promise {}'.format(self.pk)