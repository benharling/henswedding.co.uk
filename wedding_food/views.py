from django.shortcuts import render
from django.views.generic import View, FormView, CreateView, UpdateView, DetailView

from django import forms
from .models import Dish, Promise
# Create your views here.
from django_ajax.mixin import AJAXMixin
from django.core.urlresolvers import reverse
from django.core.mail import EmailMessage, send_mail
from django.template.loader import get_template


class PromiseForm(forms.Form):
    dish = forms.ModelChoiceField(queryset=Dish.objects.all())



class PromiseDetailForm(forms.ModelForm):
    dish = forms.ModelChoiceField(
        queryset=Dish.objects.all(),
        widget=forms.HiddenInput()
    )

    name = forms.CharField(label="Your name", required=False)
    email = forms.EmailField(label="Your Email", required=False)

    quantity = forms.IntegerField(label="I'll bring this many lots of this recipe", required=True, initial=1)

    class Meta:
        model = Promise
        fields = '__all__'
    

class PromiseDish(AJAXMixin, FormView):
    form_class = PromiseForm
    template_name = 'home.html'

    def form_invalid(self, form):
        return {'error':form.errors}

    def form_valid(self, form):

        dish = form.cleaned_data['dish']

        return {
            'count' : dish.available,
            "dish_name": dish.title,
            "confirm_url": reverse('confirm-promise')
        }

class ConfirmPromise(AJAXMixin, FormView):
    form_class = PromiseDetailForm
    template_name = 'home.html'

    def form_invalid(self, form):
        return form.errors

    def form_valid(self, form):

        quantity = form.cleaned_data.get('quantity')

        dish = form.cleaned_data.get('dish')

        quantity = max(1, min(dish.available, quantity))

        promises = []

        for i in range(quantity):
            promise = Promise(
                    email=form.cleaned_data.get('email'),
                    name=form.cleaned_data.get('name'),
                    dish=form.cleaned_data.get('dish')
            )
            promises.append(promise)
        Promise.objects.bulk_create(promises)

        if bool(form.cleaned_data['email']):
            subject = "[Ben&Jo's Wedding] Thanks for volunteering to cook!"
            t = get_template('food/thankyou-email.txt')
            c = {'promise': promise, 'quantity': quantity}
            message = t.render(c)
            from_email = 'Ben & Jo <blrharling@gmail.com>'
            to_email = [promise.email]

            email = EmailMessage(
                subject=subject,
                body=message,
                from_email=from_email,
                to=to_email,
                bcc=['blrharling@gmail.com', 'joannacc@hotmail.co.uk']
            )
            email.send()

        dish.refresh_from_db()
        return {
            "dish": dish.pk,
            "available": dish.available
        }

class DishDetailView(DetailView):
    model = Dish

    def get_context_data(self, **kwargs):
        context = super(DishDetailView, self).get_context_data()
        context['promise_form'] = PromiseDetailForm()
        return context