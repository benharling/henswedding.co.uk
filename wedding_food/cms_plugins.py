from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from .models import Dish, Promise

from .views import PromiseForm, PromiseDetailForm


class DishesPlugin(CMSPluginBase):

	model = CMSPlugin
	render_template = 'food/food-plugin.html'
	cache = False

	def render(self, context, instance, placeholder):
		context['dishes'] = Dish.objects.all()
		context['promise_form'] = PromiseDetailForm()
		return context


plugin_pool.register_plugin(DishesPlugin)
