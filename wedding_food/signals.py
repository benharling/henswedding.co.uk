from django.db.models.signals import pre_save
from django.dispatch import receiver
from wedding_food.models import Dish, Promise
from wedding_food import opengraph
from filer.models.foldermodels import Folder
from filer.models.imagemodels import Image
import requests
import os
from django.contrib.auth.models import User
from django.core.files import File as DjangoFile
import tempfile

import logging

logger = logging.getLogger(__file__)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}


def get_og_image(img_url):
	if img_url:
		r = requests.get(img_url, headers=headers, stream=True)
		if r.status_code == 200:

			folder, created = Folder.objects.get_or_create(name="Dishes")
			name = os.path.basename(img_url)

			lf = tempfile.NamedTemporaryFile()

			for block in r.iter_content(1024 * 8):
				if not block:
					break
				lf.write(block)

			file_obj = DjangoFile(lf, name=name)

			
			img = Image.objects.create(
				folder=folder,
				original_filename=name,
				file=file_obj
			)
			return img
	return None


@receiver(pre_save, sender=Dish)
def get_og_data(sender, instance, *args, **kwargs):

	if instance.link:
		req = requests.get(instance.link, headers=headers)
		data = opengraph.OpenGraph(scrape=True)
		try:
			data.parser(req.content)
			if data.is_valid():
				instance.title = instance.title or data.get('title', '')
				instance.description = instance.description or data.get('description', '')
				instance.image = instance.image or get_og_image(data.get('image', None))
		except:
			pass




