from __future__ import unicode_literals

from django.apps import AppConfig



class WeddingFoodConfig(AppConfig):
    name = 'wedding_food'

    def ready(self):
    	from wedding_food.signals import get_og_data
